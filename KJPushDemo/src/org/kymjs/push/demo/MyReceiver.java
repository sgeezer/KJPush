package org.kymjs.push.demo;

import org.kymjs.kjframe.utils.KJLoger;
import org.kymjs.push.core.PushReceiver;

import android.content.Context;
import android.content.Intent;

public class MyReceiver extends PushReceiver {

    @Override
    public void onNetworkStateChange(Context context, Intent intent) {
        KJLoger.debug("网络改变");
    }

    @Override
    public void onScreenOff(Context context, Intent intent) {
        KJLoger.debug("屏幕关闭");
    }

    @Override
    public void onScreenOn(Context context, Intent intent) {
        KJLoger.debug("屏幕开启");
    }

    @Override
    public void onPullData(Context context, Intent intent) {
        KJLoger.debug("请求服务器");
    }

}
